<?php

namespace NoMercy\AdminSeeder;

use App\User;
use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Spatie\Permission\Models\Role;

class AdminSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $file = 'users.json';
        if (Storage::disk('local')->exists($file)) {
            $file = json_decode(Storage::disk('local')->get($file), true);
            foreach ($file as $data) {

                $user = User::create([
                    "id" => $data['id'],
                    "default_language" => $data['default_language'],
                    "email" => $data['email'],
                    "email_verified_at" => $data['email_verified_at'],
                    "name" => $data['name'],
                    "public_room_name" => $data['public_room_name'],
                    "password" => $data['password'],
                    "api_token" => $data['api_token'],
                    "skipIntro" => $data['skipIntro'],
                    "status_id" => $data['status_id'],
                    "verified" => $data['verified'],
                    "remember_token" => $data['remember_token'],
                    "created_at" => $data['created_at'],
                    "updated_at" => $data['updated_at'],
                    "last_login_at" => $data['last_login_at'],
                    "last_login_ip" => $data['last_login_ip'],
                    "last_request_at" => $data['last_request_at'],
                    "last_request_ip" => $data['last_request_ip'],
                ]);
                $user->syncRoles($data['roles']);
            }
        }
        else {
            DB::table('users')->delete();
            $user =  User::create([
                'name'           => 'Admin',
                'email'          => env('ADMIN_EMAIL', 'admin@example.com'),
                'password'       => bcrypt(env('ADMIN_PASSWORD', 'password')),
                'api_token'      => env('ADMIN_API_TOKEN', str_random(60)),
                'email_verified_at'    => Carbon::now(),
            ]);
            $user->syncRoles(Role::all());
        }
    }
}
