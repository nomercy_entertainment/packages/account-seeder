# Admin account seeder for Laravel
<p>
  <img alt="Version" src="https://img.shields.io/badge/version-1.0.0-blue.svg?cacheSeconds=2592000" />
  
</p>

### This package creates an admin account with .env credentials.
### It saves you creating your account after every migrate:fresh command.


## Install

```sh
composer require nomercy/adminseeder
```
## add this  to your DatabaseSeeder class.
```php
$this->call(AdminSeeder::class);
```
## Create these entries in your .env file.
By default it creates a user with admin@example.com, password and a random 60 string
```
ADMIN_EMAIL=
ADMIN_PASSWORD=
ADMIN_API_TOKEN=
```
```sh
php artisan migrate:fresh --seed
```
## Author

## 👤 **Stoney_Eagle**

### * Twitter: [@Stoney_Eagle](https://twitter.com/Stoney_Eagle)
### * Gitlab: [@Stoney_Eagle](https://gitlab.com/Stoney_Eagle)

